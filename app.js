const EventEmitter = (Base) => class EventEmitter extends Base {
    handlersFor = {};

    on(eventName, handler) {
        if (!this.handlersFor[eventName]) {
            this.handlersFor[eventName] = new Set();
        }
        this.handlersFor[eventName].add(handler);
    }

    off(eventName, handler) {
        const handlers = this.handlersFor[eventName];
        if (handlers) {
            handlers.delete(handler);
        }
    }

    emit(eventName, ...params) {
        const handlers = this.handlersFor[eventName];
        if (handlers) {
            for (const handler of handlers) {
                handler(...params);
            }
        }
    }
}

class Program {
    sidebar = document.getElementById('sidebar');
    editButton = document.querySelector('#header .edit-button');
    sidebarButton = document.querySelector('#header .sidebar-button');
    content = document.getElementById('content');
    cardContainer = document.querySelector('#content .question-cards');

    questions;
    components;
    elements;
    linkElements;
    mode = 'view';

    addButton;

    constructor() {
        this.setUpQuestions();
        this.rebuildComponentsAndElements();
        this.setUpSidebar();
        this.setUpModeChange();
        this.setUpAddButton();
        this.render();
    }

    setUpQuestions() {
        this.loadQuestions();
        window.addEventListener('beforeunload', () => this.saveQuestions());
    }

    saveQuestions() {
        localStorage.setItem('questions', JSON.stringify(this.questions));
    }

    loadQuestions() {
        const savedQuestions = localStorage.getItem('questions');
        this.questions = savedQuestions ? JSON.parse(savedQuestions) : this.originalQuestions;
    }

    get originalQuestions() {
        return [
            {
                question: "Can you answer these questions?",
                answer: "I'm sure you can! Good luck",
                created_at: 1541944069,
                rate_stars: 5
            },
            {
                question: "In CSS what is the difference between margin and padding",
                answer: "Margin is outside of an element, padding is inside.",
                created_at: 1541944069,
                rate_stars: 4
            },
            {
                question: "What responsive web design means? ",
                answer: "The UI of a site adapts to the device it's displayed on",
                created_at: 1541944069,
                rate_stars: 4
            },
            {
                question: "What is JSON?",
                answer: "A data format based on JS syntax commonly used in HTTP requests.",
                created_at: 1541944069,
                rate_stars: 4
            },
            {
                question: "What is event binding?",
                answer: "Registering handlers for events I guess, but I don't use this terminology.",
                created_at: 1541944069,
                rate_stars: 4
            },
            {
                question: "What is a Promise?",
                answer: "A type representing eventually available data. Helps preventing callback-hell. Works like a monad, but doesn't respect all monad laws.",
                created_at: 1541944069,
                rate_stars: 4
            },
            {
                question: "What does non-blocking I/O means and why is that matter in User Interfaces?",
                answer: "IO runs asynchronously, so the UI doesn't freeze while waiting for input or reading a long file.",
                created_at: 1541944069,
                rate_stars: 4
            },
            {
                question: "What is Babel and why you should know about it?",
                answer: "A JS transpiler. It's main purpose is to turn modern unsupported JS syntax to supported syntax, but can also do e.g. polyfilling, macros.",
                created_at: 1541944069,
                rate_stars: 4
            },
            {
                question: "What is webpack?",
                answer: "A JS module bundler. Turn JS modules into browser-compatible files. It's great, because it can handle other file types too, so every asset can be part o an explicit dependency graph.",
                created_at: 1541944069,
                rate_stars: 4
            },
            {
                question: "How can you cut a round cheese three times to make eight equal slices?",
                answer: "Cut it into quarter-circles with two cuts, then cut it in half horizontally.",
                created_at: 1541944069,
                rate_stars: 4
            },
            {
                question: "Which framework is the best?",
                answer: "Whichever gets a particular job done the best.",
                created_at: 1541944069,
                rate_stars: 4
            },
        ];
    }

    setUpAddButton() {
        this.addButton = document.createElement('button');
        this.addButton.classList.add('add-button');
        this.addButton.innerHTML = 'Add question';
        this.addButton.addEventListener('click', () => this.addQuestion());
    }

    setUpSidebar() {
        this.sidebarButton.addEventListener('click', () => sidebar.classList.toggle('shown'));
    }

    setUpModeChange() {
        this.editButton.addEventListener('click', () => this.toggleMode());
    }

    toggleMode() {
        const newMode = this.mode === 'view' ? 'edit' : 'view';
        this.mode = newMode;

        if (newMode === 'edit') {
            this.content.appendChild(this.addButton);
        } else {
            this.content.removeChild(this.addButton);
        }

        this.setModeOfComponents(newMode);
    };

    setModeOfComponents(mode) {
        for (const component of this.components) {
            component.mode = mode;
        }
    }

    render() {
        this.cardContainer.innerHTML = '';
        this.sidebar.innerHTML = '';

        for (const element of this.elements) {
            this.cardContainer.appendChild(element);
        }

        for (const linkElement of this.linkElements) {
            this.sidebar.appendChild(linkElement);
        }
    }

    getComponentsFromQuestions() {
        this.components =  this.questions.map((question, index) => new QuestionComponent(index, question));
        this.setModeOfComponents(this.mode);
        this.subscribeToEventsOfAllComponents();
    }

    getElementsFromComponents() {
        this.elements = this.components.map(component => component.element);
    }

    getLinkElementsFromComponents() {
        this.linkElements = this.components.map(component => component.linkElement);
    }

    rebuildComponentsAndElements() {
        this.getComponentsFromQuestions();
        this.getElementsFromComponents();
        this.getLinkElementsFromComponents();
    }

    subscribeToEventsOfAllComponents() {
        for (const component of this.components) {
            this.subscribeToEventsOf(component);
        }
    }

    subscribeToEventsOf(component) {
        component.on('elementchange', () => this.render());
        component.on('questionchange', (index, question) => this.editQuestion(index, question));
        component.on('delete', (index) => this.deleteQuestion(index));
    }

    addQuestion() {
        const question = {
            question: '???',
            answer: '???',
            created_at: Date.now(),
            rate_stars: 3,
        };
        this.questions.push(question);
        const component = new QuestionComponent(this.questions.length - 1, question);
        component.mode = 'edit-content';
        this.components.push(component);
        this.subscribeToEventsOf(component);
        this.getElementsFromComponents();
        this.getLinkElementsFromComponents();
        this.render();
    }

    editQuestion(index, newQuestion) {
        const question = this.questions[index];
        this.questions[index] = { ...question, ...newQuestion };

        this.rebuildComponentsAndElements();
        this.render();
    }

    deleteQuestion(index) {
        this.questions.splice(index, 1);
        this.rebuildComponentsAndElements();
        this.render();
    }
}

class QuestionComponent extends EventEmitter(Object) {
    index;
    question;
    element;
    linkElement;
    _mode;
    content = document.getElementById('content');

    constructor(index, question) {
        super();
        this.index = index;
        this.question = question;

        this.element = document.createElement('div');
        this.element.classList.add('question-card');

        this.linkElement = document.createElement('div');
        this.linkElement.classList.add('question-link');

        this.render();
    }

    set mode(mode) {
        this._mode = mode;
        this.render();
    }

    render() {
        const {
            question,
            answer,
            created_at,
            rate_stars,
        } = this.question;

        this.element.innerHTML = `
            <div class="header">
                <div class="question">Q: <span class="text" ${this._mode === 'edit-content' ? 'contenteditable' : ''}>${question}</span></div>
                <div class="buttons">
                    ${this._mode === 'edit' ?
                        '<button class="edit-button">edit</button>' :
                     this._mode === 'edit-content'?
                        '<button class="edit-button">done</button>'
                    : ''}
                    ${this._mode === 'edit' || this._mode === 'edit-content' ? '<button class="remove-button">remove</button>' : ''}
                </div>
            </div>
            <div class="answer">A: <span class="text" ${this._mode === 'edit-content' ? 'contenteditable' : ''}>${answer}</span></div>
            <div class="other-data">
                <div class="created-at">Created at: <span>${this.formatTimestamp(created_at)}</span></div>
                <div class="star-rating">
                    ${'<div class="star full"></div>'.repeat(rate_stars)}
                    ${'<div class="star"></div>'.repeat(5 - rate_stars)}
                </div>
            </div>
        `;

        if (this._mode === 'edit' || this._mode === 'edit-content') {
            const editButton = this.element.querySelector('.edit-button');
            const removeButton = this.element.querySelector('.remove-button');
            removeButton.addEventListener('click', () => this.emit('delete', this.index));
            editButton.addEventListener('click', () => this.toggleEditContentMode());
        }

        this.renderLink();

        this.emit('elementchange');
    }

    renderLink() {
        this.linkElement.innerHTML = this.question.question;
        this.linkElement.addEventListener('click', () => {
            content.scroll({ top: this.element.offsetTop - 68 });
        });
    }

    formatTimestamp(timestamp) {
        const date = new Date(timestamp);
        const year = date.getFullYear();
        const month = String(date.getMonth() + 1).padStart(2, '0');
        const day = String(date.getDate()).padStart(2, '0');
        return `${year}.${month}.${day}`;
    }

    toggleEditContentMode() {
        const oldMode = this._mode;

        if (oldMode === 'edit-content') {
            const question = this.element.querySelector('.question .text').innerHTML;
            const answer = this.element.querySelector('.answer .text').innerHTML;
            const newQuestion = { question,  answer };
            this.emit('questionchange', this.index, newQuestion);
        }

        this.mode = oldMode === 'edit' ? 'edit-content' : 'edit';
    }
}

const program = new Program();
